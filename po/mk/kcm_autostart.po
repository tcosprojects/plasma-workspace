# translation of kcm_autostart.po to Macedonian
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Bozidar Proevski <bobibobi@freemail.com.mk>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: kcm_autostart\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-20 02:12+0000\n"
"PO-Revision-Date: 2008-12-21 16:51+0100\n"
"Last-Translator: Bozidar Proevski <bobibobi@freemail.com.mk>\n"
"Language-Team: Macedonian <mkde-l10n@lists.sourceforge.net>\n"
"Language: mk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: Plural-Forms: nplurals=3; plural=n%10==1 ? 0 : n%10==2 ? 1 : "
"2;\n"

#: autostartmodel.cpp:328
#, kde-format
msgid "\"%1\" is not an absolute url."
msgstr ""

#: autostartmodel.cpp:331
#, kde-format
msgid "\"%1\" does not exist."
msgstr ""

#: autostartmodel.cpp:334
#, kde-format
msgid "\"%1\" is not a file."
msgstr ""

#: autostartmodel.cpp:337
#, kde-format
msgid "\"%1\" is not readable."
msgstr ""

#: ui/main.qml:41
#, kde-format
msgid "Make Executable"
msgstr ""

#: ui/main.qml:55
#, kde-format
msgid "The file '%1' must be executable to run at logout."
msgstr ""

#: ui/main.qml:57
#, kde-format
msgid "The file '%1' must be executable to run at login."
msgstr ""

#: ui/main.qml:95
#, fuzzy, kde-format
#| msgid "&Properties"
msgctxt "@action:button"
msgid "See properties"
msgstr "&Својства"

#: ui/main.qml:101
#, fuzzy, kde-format
#| msgid "&Remove"
msgctxt "@action:button"
msgid "Remove entry"
msgstr "Отст&рани"

#: ui/main.qml:112
#, kde-format
msgid "Applications"
msgstr ""

#: ui/main.qml:115
#, kde-format
msgid "Login Scripts"
msgstr ""

#: ui/main.qml:118
#, fuzzy, kde-format
#| msgid "Pre-KDE startup"
msgid "Pre-startup Scripts"
msgstr "Стартување пред KDE"

#: ui/main.qml:121
#, kde-format
msgid "Logout Scripts"
msgstr ""

#: ui/main.qml:130
#, kde-format
msgid "No user-specified autostart items"
msgstr ""

#: ui/main.qml:131
#, kde-kuit-format
msgctxt "@info"
msgid "Click the <interface>Add…</interface> button below to add some"
msgstr ""

#: ui/main.qml:145
#, fuzzy, kde-format
#| msgid "Add Script..."
msgid "Choose Login Script"
msgstr "Додавање скрипта..."

#: ui/main.qml:165
#, kde-format
msgid "Choose Logout Script"
msgstr ""

#: ui/main.qml:182
#, kde-format
msgid "Add…"
msgstr ""

#: ui/main.qml:197
#, kde-format
msgid "Add Application…"
msgstr ""

#: ui/main.qml:203
#, fuzzy, kde-format
#| msgid "Add Script..."
msgid "Add Login Script…"
msgstr "Додавање скрипта..."

#: ui/main.qml:209
#, fuzzy, kde-format
#| msgid "Add Script..."
msgid "Add Logout Script…"
msgstr "Додавање скрипта..."

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Божидар Проевски"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "bobibobi@freemail.com.mk"

#, fuzzy
#~| msgid "KDE Autostart Manager Control Panel Module"
#~ msgid "Session Autostart Manager Control Panel Module"
#~ msgstr "Модул „Менаџер за автом. старт во KDE“ за контролниот панел"

#, fuzzy
#~| msgid "(c) 2006-2007-2008 Autostart Manager team"
#~ msgid "Copyright © 2006–2020 Autostart Manager team"
#~ msgstr "(c) 2006-2007-2008 тимот на менаџерот за автом. старт"

#~ msgid "Stephen Leaf"
#~ msgstr "Stephen Leaf"

#~ msgid "Montel Laurent"
#~ msgstr "Montel Laurent"

#~ msgid "Maintainer"
#~ msgstr "Одржувач"

#, fuzzy
#~| msgid "Advanced"
#~ msgid "Add..."
#~ msgstr "Напредно"

#, fuzzy
#~| msgid "Shell script:"
#~ msgid "Shell script path:"
#~ msgstr "Скрипта за школка:"

#~ msgid "Create as symlink"
#~ msgstr "Креирај како симболичка врска"

#, fuzzy
#~| msgid "Autostart only in KDE"
#~ msgid "Autostart only in Plasma"
#~ msgstr "Автом. старт само во KDE"

#~ msgid "Name"
#~ msgstr "Име"

#~ msgid "Command"
#~ msgstr "Наредба"

#~ msgid "Status"
#~ msgstr "Статус"

#, fuzzy
#~| msgctxt ""
#~| "@title:column The name of the column that decides if the program is run "
#~| "on kde startup, on kde shutdown, etc"
#~| msgid "Run On"
#~ msgctxt ""
#~ "@title:column The name of the column that decides if the program is run "
#~ "on session startup, on session shutdown, etc"
#~ msgid "Run On"
#~ msgstr "Изврши при"

#, fuzzy
#~| msgid "KDE Autostart Manager"
#~ msgid "Session Autostart Manager"
#~ msgstr "Менаџер за автом. старт во KDE"

#~ msgctxt "The program will be run"
#~ msgid "Enabled"
#~ msgstr "Овозможено"

#~ msgctxt "The program won't be run"
#~ msgid "Disabled"
#~ msgstr "Оневозможено"

#~ msgid "Desktop File"
#~ msgstr "Desktop-датотека"

#~ msgid "Script File"
#~ msgstr "Датотека со скрипта"

#~ msgid "Add Program..."
#~ msgstr "Додавање програма..."

#~ msgid "Startup"
#~ msgstr "Стартување"

#, fuzzy
#~| msgid ""
#~| "KDE only reads files with sh extensions for setting up the environment."
#~ msgid ""
#~ "Only files with “.sh” extensions are allowed for setting up the "
#~ "environment."
#~ msgstr "KDE чита датотеки само со наставка .sh за поставување на околината."

#~ msgid "Shutdown"
#~ msgstr "Спуштање"
