# Translation of krunner.po to Catalan
# Copyright (C) 2014-2023 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2014, 2015, 2016, 2020.
# Josep M. Ferrer <txemaq@gmail.com>, 2017, 2019, 2021, 2022, 2023.
# Empar Montoro Martín <montoro_mde@gva.es>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-19 03:24+0000\n"
"PO-Revision-Date: 2023-07-19 20:07+0300\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Generator: Lokalize 22.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Antoni Bella"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "antonibella5@yahoo.com"

#: main.cpp:75 view.cpp:47
#, kde-format
msgid "KRunner"
msgstr "KRunner"

#: main.cpp:75
#, kde-format
msgid "Run Command interface"
msgstr "Interfície d'execució ordres"

#: main.cpp:81
#, kde-format
msgid "Use the clipboard contents as query for KRunner"
msgstr "Usa el contingut del porta-retalls com a consulta per al KRunner"

#: main.cpp:82
#, kde-format
msgid "Start KRunner in the background, don't show it."
msgstr "Inicia el KRunner en segon pla, no el mostris."

#: main.cpp:83
#, kde-format
msgid "Replace an existing instance"
msgstr "Substitueix una instància existent"

#: main.cpp:84
#, kde-format
msgid "Only query this specific runner"
msgstr "Consulta només aquest executor específic"

#: main.cpp:90
#, kde-format
msgid "The query to run, only used if -c is not provided"
msgstr "La consulta a executar, només s'usa si no es proporciona «-c»"

#: qml/RunCommand.qml:94
#, kde-format
msgid "Configure"
msgstr "Configura"

#: qml/RunCommand.qml:95
#, kde-format
msgid "Configure KRunner Behavior"
msgstr "Configura el comportament del KRunner"

#: qml/RunCommand.qml:98
#, kde-format
msgid "Configure KRunner…"
msgstr "Configura el KRunner…"

#: qml/RunCommand.qml:111
#, kde-format
msgid "Querying runner %1 in single runner mode"
msgstr "S'està consulta l'executor %1 en mode d'executor únic"

#: qml/RunCommand.qml:125
#, kde-format
msgctxt "Textfield placeholder text, query specific KRunner plugin"
msgid "Search '%1'…"
msgstr "Cerca «%1»…"

#: qml/RunCommand.qml:126
#, kde-format
msgctxt "Textfield placeholder text"
msgid "Search…"
msgstr "Cerca…"

#: qml/RunCommand.qml:300 qml/RunCommand.qml:301 qml/RunCommand.qml:303
#, kde-format
msgid "Show Usage Help"
msgstr "Mostra l'ajuda d'ús"

#: qml/RunCommand.qml:311
#, kde-format
msgid "Pin"
msgstr "Fixa"

#: qml/RunCommand.qml:312
#, kde-format
msgid "Pin Search"
msgstr "Fixa la cerca"

#: qml/RunCommand.qml:314
#, kde-format
msgid "Keep Open"
msgstr "Mantén obert"

#: qml/RunCommand.qml:386 qml/RunCommand.qml:391
#, kde-format
msgid "Recent Queries"
msgstr "Consultes recents"

#: qml/RunCommand.qml:389
#, kde-format
msgid "Remove"
msgstr "Elimina"
