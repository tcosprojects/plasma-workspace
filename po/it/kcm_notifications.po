# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
# Paolo Zamponi <feus73@gmail.com>, 2019, 2020, 2021, 2022, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-18 02:10+0000\n"
"PO-Revision-Date: 2023-01-03 08:43+0100\n"
"Last-Translator: Paolo Zamponi <zapaolo@email.it>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.0\n"

#: kcm.cpp:64
#, kde-format
msgid "Toggle do not disturb"
msgstr "Attiva/disattiva non disturbare"

#: kcm.cpp:192
#, kde-format
msgid "Configure Notifications"
msgstr "Configura le notifiche"

#: sourcesmodel.cpp:364
#, kde-format
msgid "Other Applications"
msgstr "Altre applicazioni"

#: ui/ApplicationConfiguration.qml:62
#, kde-format
msgid "Show popups"
msgstr "Mostra finestre a comparsa"

#: ui/ApplicationConfiguration.qml:76
#, kde-format
msgid "Show in do not disturb mode"
msgstr "Mostra in modalità non disturbare"

#: ui/ApplicationConfiguration.qml:89 ui/main.qml:147
#, kde-format
msgid "Show in history"
msgstr "Mostra nella cronologia"

#: ui/ApplicationConfiguration.qml:100
#, kde-format
msgid "Show notification badges"
msgstr "Mostra simboli di notifica"

#: ui/ApplicationConfiguration.qml:118
#, kde-format
msgid "Configure Events…"
msgstr "Configura eventi…"

#: ui/ApplicationConfiguration.qml:129
#, kde-format
msgid ""
"This application does not support configuring notifications on a per-event "
"basis."
msgstr ""
"Questa applicazione non supporta la configurazione delle notifiche per ogni "
"singolo evento."

#: ui/main.qml:41
#, kde-format
msgid ""
"Could not find a 'Notifications' widget, which is required for displaying "
"notifications. Make sure that it is enabled either in your System Tray or as "
"a standalone widget."
msgstr ""
"Impossibile trovare un oggetto «Notifiche», che è richiesto per visualizzare "
"le notifiche. Assicurati che sia abilitato nel vassoio di sistema oppure "
"come oggetto indipendente."

#: ui/main.qml:52
#, kde-format
msgctxt "Vendor and product name"
msgid "Notifications are currently provided by '%1 %2' instead of Plasma."
msgstr "Le notifiche sono attualmente fornite da «%1 %2» invece che da Plasma."

#: ui/main.qml:56
#, kde-format
msgid "Notifications are currently not provided by Plasma."
msgstr "Le notifiche non sono attualmente fornite da Plasma."

#: ui/main.qml:63
#, kde-format
msgctxt "@title:group"
msgid "Do Not Disturb mode"
msgstr "Modalità non disturbare"

#: ui/main.qml:68
#, kde-format
msgctxt "Enable Do Not Disturb mode when screens are mirrored"
msgid "Enable:"
msgstr "Abilita:"

#: ui/main.qml:69
#, kde-format
msgctxt "Enable Do Not Disturb mode when screens are mirrored"
msgid "When screens are mirrored"
msgstr "Quando gli schermi sono duplicati"

#: ui/main.qml:81
#, kde-format
msgctxt "Enable Do Not Disturb mode during screen sharing"
msgid "During screen sharing"
msgstr "Durante la condivisione dello schermo"

#: ui/main.qml:96
#, kde-format
msgctxt "Keyboard shortcut to turn Do Not Disturb mode on and off"
msgid "Keyboard shortcut:"
msgstr "Scorciatoia da tastiera:"

#: ui/main.qml:103
#, kde-format
msgctxt "@title:group"
msgid "Visibility conditions"
msgstr "Condizioni di visibilità"

#: ui/main.qml:108
#, kde-format
msgid "Critical notifications:"
msgstr "Notifiche critiche:"

#: ui/main.qml:109
#, kde-format
msgid "Show in Do Not Disturb mode"
msgstr "Mostra in modalità non disturbare"

#: ui/main.qml:121
#, kde-format
msgid "Normal notifications:"
msgstr "Notifiche normali:"

#: ui/main.qml:122
#, kde-format
msgid "Show over full screen windows"
msgstr "Mostra sopra le finestre a tutto schermo"

#: ui/main.qml:134
#, kde-format
msgid "Low priority notifications:"
msgstr "Notifiche a bassa priorità:"

#: ui/main.qml:135
#, kde-format
msgid "Show popup"
msgstr "Mostra finestra a comparsa"

#: ui/main.qml:164
#, kde-format
msgctxt "@title:group As in: 'notification popups'"
msgid "Popups"
msgstr "Finestre a comparsa"

#: ui/main.qml:170
#, kde-format
msgctxt "@label"
msgid "Location:"
msgstr "Posizione:"

#: ui/main.qml:171
#, kde-format
msgctxt "Popup position near notification plasmoid"
msgid "Near notification icon"
msgstr "Vicino all'icona di notifica"

#: ui/main.qml:208
#, kde-format
msgid "Choose Custom Position…"
msgstr "Scegli posizione personalizzata…"

#: ui/main.qml:217 ui/main.qml:233
#, kde-format
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "un secondo"
msgstr[1] "%1 secondi"

#: ui/main.qml:222
#, kde-format
msgctxt "Part of a sentence like, 'Hide popup after n seconds'"
msgid "Hide after:"
msgstr "Nascondi dopo:"

#: ui/main.qml:245
#, kde-format
msgctxt "@title:group"
msgid "Additional feedback"
msgstr "Riscontro aggiuntivo"

#: ui/main.qml:250
#, kde-format
msgid "Application progress:"
msgstr "Avanzamento dell'applicazione:"

#: ui/main.qml:251 ui/main.qml:291
#, kde-format
msgid "Show in task manager"
msgstr "Mostra nel gestore dei processi"

#: ui/main.qml:263
#, kde-format
msgctxt "Show application jobs in notification widget"
msgid "Show in notifications"
msgstr "Mostra nelle notifiche"

#: ui/main.qml:277
#, kde-format
msgctxt "Keep application job popup open for entire duration of job"
msgid "Keep popup open during progress"
msgstr "Mantieni aperta la finestra a comparsa durante l'avanzamento"

#: ui/main.qml:290
#, kde-format
msgid "Notification badges:"
msgstr "Simboli di notifica:"

#: ui/main.qml:302
#, kde-format
msgctxt "@title:group"
msgid "Application-specific settings"
msgstr "Impostazioni specifiche delle applicazioni"

#: ui/main.qml:307
#, kde-format
msgid "Configure…"
msgstr "Configura…"

#: ui/PopupPositionPage.qml:14
#, kde-format
msgid "Popup Position"
msgstr "Posizione della finestra a comparsa"

#: ui/SourcesPage.qml:18
#, kde-format
msgid "Application Settings"
msgstr "Impostazioni delle applicazioni"

#: ui/SourcesPage.qml:92
#, kde-format
msgid "Applications"
msgstr "Applicazioni"

#: ui/SourcesPage.qml:93
#, kde-format
msgid "System Services"
msgstr "Servizi di sistema"

#: ui/SourcesPage.qml:131
#, kde-format
msgid "No application or event matches your search term."
msgstr "Nessuna applicazione o evento corrisponde ai termini di ricerca."

#: ui/SourcesPage.qml:151
#, kde-format
msgid ""
"Select an application from the list to configure its notification settings "
"and behavior."
msgstr ""
"Seleziona un'applicazione dalla lista per configurare le sue impostazioni di "
"notifica e il suo comportamento."

#~ msgid "This module lets you manage application and system notifications."
#~ msgstr ""
#~ "Questo modulo ti permette di gestire le notifiche delle applicazioni e "
#~ "del sistema."

#~ msgctxt "Turn do not disturb mode on/off with keyboard shortcut"
#~ msgid "Toggle with:"
#~ msgstr "Cambia con:"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Paolo Zamponi"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "zapaolo@email.it"

#~ msgid "Notifications"
#~ msgstr "Notifiche"

#~ msgid "Kai Uwe Broulik"
#~ msgstr "Kai Uwe Broulik"

#~ msgid "Show critical notifications"
#~ msgstr "Mostra notifiche critiche"

#~ msgid "Always keep on top"
#~ msgstr "Tieni sempre sopra"

#~ msgid "Popup position:"
#~ msgstr "Posizione della finestra a comparsa:"
