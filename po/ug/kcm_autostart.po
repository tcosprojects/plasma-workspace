# Uyghur translation for kcm_autostart.
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Sahran <sahran.ug@gmail.com>, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: kcm_autostart\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-20 02:12+0000\n"
"PO-Revision-Date: 2013-09-08 07:05+0900\n"
"Last-Translator: Gheyret Kenji <gheyret@gmail.com>\n"
"Language-Team: Uyghur Computer Science Association <UKIJ@yahoogroups.com>\n"
"Language: ug\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: autostartmodel.cpp:328
#, kde-format
msgid "\"%1\" is not an absolute url."
msgstr ""

#: autostartmodel.cpp:331
#, kde-format
msgid "\"%1\" does not exist."
msgstr ""

#: autostartmodel.cpp:334
#, kde-format
msgid "\"%1\" is not a file."
msgstr ""

#: autostartmodel.cpp:337
#, kde-format
msgid "\"%1\" is not readable."
msgstr ""

#: ui/main.qml:41
#, kde-format
msgid "Make Executable"
msgstr ""

#: ui/main.qml:55
#, kde-format
msgid "The file '%1' must be executable to run at logout."
msgstr ""

#: ui/main.qml:57
#, kde-format
msgid "The file '%1' must be executable to run at login."
msgstr ""

#: ui/main.qml:95
#, fuzzy, kde-format
#| msgid "&Properties..."
msgctxt "@action:button"
msgid "See properties"
msgstr "خاسلىق(&P)…"

#: ui/main.qml:101
#, fuzzy, kde-format
#| msgid "&Remove"
msgctxt "@action:button"
msgid "Remove entry"
msgstr "چىقىرىۋەت(&R)"

#: ui/main.qml:112
#, kde-format
msgid "Applications"
msgstr ""

#: ui/main.qml:115
#, kde-format
msgid "Login Scripts"
msgstr ""

#: ui/main.qml:118
#, kde-format
msgid "Pre-startup Scripts"
msgstr ""

#: ui/main.qml:121
#, kde-format
msgid "Logout Scripts"
msgstr ""

#: ui/main.qml:130
#, kde-format
msgid "No user-specified autostart items"
msgstr ""

#: ui/main.qml:131
#, kde-kuit-format
msgctxt "@info"
msgid "Click the <interface>Add…</interface> button below to add some"
msgstr ""

#: ui/main.qml:145
#, fuzzy, kde-format
#| msgid "Add Script..."
msgid "Choose Login Script"
msgstr "قوليازما قوش…"

#: ui/main.qml:165
#, kde-format
msgid "Choose Logout Script"
msgstr ""

#: ui/main.qml:182
#, kde-format
msgid "Add…"
msgstr ""

#: ui/main.qml:197
#, kde-format
msgid "Add Application…"
msgstr ""

#: ui/main.qml:203
#, fuzzy, kde-format
#| msgid "Add Script..."
msgid "Add Login Script…"
msgstr "قوليازما قوش…"

#: ui/main.qml:209
#, fuzzy, kde-format
#| msgid "Add Script..."
msgid "Add Logout Script…"
msgstr "قوليازما قوش…"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "ئابدۇقادىر ئابلىز, غەيرەت كەنجى"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "sahran.ug@gmail.com,  gheyret@gmail.com"

#, fuzzy
#~| msgid "Copyright © 2006–2010 Autostart Manager team"
#~ msgid "Copyright © 2006–2020 Autostart Manager team"
#~ msgstr ""
#~ "Copyright © 2006–2010 ئاپتوماتىك باشلاشنى باشقۇرغۇچ(Autostart Manager) "
#~ "ئەترىتى"

#~ msgid "Stephen Leaf"
#~ msgstr "Stephen Leaf"

#~ msgid "Montel Laurent"
#~ msgstr "Montel Laurent"

#~ msgid "Maintainer"
#~ msgstr "مەسئۇل كىشى"

#, fuzzy
#~| msgid "Advanced..."
#~ msgid "Add..."
#~ msgstr "ئالىي…"

#, fuzzy
#~| msgid "Autostart only in KDE"
#~ msgid "Autostart only in Plasma"
#~ msgstr "پەقەت KDE دىلا ئاپتوماتىك باشلا"

#~ msgid "Name"
#~ msgstr "ئاتى"

#~ msgid "Command"
#~ msgstr "بۇيرۇق"

#~ msgid "Status"
#~ msgstr "ھالىتى"

#~ msgctxt "The program will be run"
#~ msgid "Enabled"
#~ msgstr "ئىناۋەتلىك قىلىنغان"

#~ msgctxt "The program won't be run"
#~ msgid "Disabled"
#~ msgstr "ئىناۋەتسىز قىلىنغان"

#~ msgid "Desktop File"
#~ msgstr "ئۈستەلئۈستى ھۆججىتى"

#~ msgid "Script File"
#~ msgstr "قوليازما ھۆججىتى"

#~ msgid "Add Program..."
#~ msgstr "پروگرامما قوش…"

#~ msgid "Startup"
#~ msgstr "قوزغىلىش"

#~ msgid "Shutdown"
#~ msgstr "تاقا"
